import './App.css';
import React, { Suspense } from 'react';
import { Route, Routes } from 'react-router-dom';

const Layout = React.lazy(() => import('./layouts/index'));

function App() {
  return (
    <Suspense fallback={<div className="bg-white h-full w-full" />}>
      <Routes>
        <Route path="/*" element={<Layout />} />
      </Routes>
    </Suspense>
  );
}

export default App;
