import React from 'react';

const PPL = React.lazy(() => import('./views/PPL'));

const routes = [
  {
    path: '/ppl',
    name: 'PPL',
    element: PPL,
  },
];

export default routes;
