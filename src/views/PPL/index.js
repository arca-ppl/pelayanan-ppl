import React, { useEffect, useRef, useState } from 'react';
import { MapContainer, TileLayer, Marker, Popup } from 'react-leaflet';
import { Circle, LocationOnRounded } from '@mui/icons-material';
import 'leaflet/dist/leaflet.css';
import { Icon } from 'leaflet';
import { Divider } from '@mui/material';
import axios from 'axios';
import { Table } from 'antd';
import { formatNumber } from './../../helpers';

const SERVER_URL = process.env.REACT_APP_SERVER_URL;

const summaryInit = {
  title: '',
  content: '',
  color: '',
};

export default function Index() {
  const params = new URLSearchParams(window.location.search);

  const [customers, setCustomers] = useState([]);
  const [customerMembers, setCustomerMembers] = useState([]);
  const [summary, setSummary] = useState([summaryInit]);

  // DISPLAY DATA
  const [markers, setMarkers] = useState([]);

  const [keywordAP, setKeywordAP] = useState(params.get('ap') || null);

  const populateSummary = (data) => {
    // POPULATE SUMMARY
    const pplAvarage = {
      title: 'Rata - rata Pelayanan PPL',
      content: `${(
        (data.reduce((acc, next) => acc + next.rate / 100, 0) / data.length) *
        100
      ).toFixed(1)}%`,
      color: 'text-blue-600',
    };

    const totalAP = {
      title: 'Total AP',
      content: `${data.length} AP`,
      color: 'text-blue-700',
    };

    const pplNinetyUntilOneHundread = {
      title: 'Pelayanan PPL 90% - 100%',
      content: `${data.filter(({ rate }) => rate >= 90).length || 0} AP`,
      color: 'text-green-500',
    };

    const pplSeventyUntilEightyNine = {
      title: 'Pelayanan PPL 70% - 89%',
      content: `${
        data.filter(({ rate }) => rate >= 70 && rate < 90).length || 0
      } AP`,
      color: 'text-orange-500',
    };

    const pplUnderSeventy = {
      title: 'Pelayanan PPL <69%',
      content: `${data.filter(({ rate }) => rate < 70).length || 0} AP`,
      color: 'text-red-500',
    };

    setSummary([
      pplAvarage,
      totalAP,
      pplNinetyUntilOneHundread,
      pplSeventyUntilEightyNine,
      pplUnderSeventy,
    ]);
  };

  const getCustomer = async () => {
    await axios
      .get(`${SERVER_URL}/api/customer`)
      .then((response) => {
        const data = response.data;
        const manipulationCustomer = data.map((ap) => ({
          ...ap,
          customer_members: ap.customer_members.map((member) => ({
            ...member,
            color:
              member.rate >= 90
                ? 'text-green-500'
                : member.rate < 70
                ? 'text-red-500'
                : 'text-orange-500',
            marker:
              member.rate >= 90
                ? 'location-on-green'
                : member.rate < 70
                ? 'location-on-red'
                : 'location-on-orange',
          })),
          color:
            ap.rate >= 90
              ? 'text-green-500'
              : ap.rate < 70
              ? 'text-red-500'
              : 'text-orange-500',
          marker:
            ap.rate >= 90
              ? 'location-on-green'
              : ap.rate < 70
              ? 'location-on-red'
              : 'location-on-orange',
        }));

        setCustomers(manipulationCustomer);

        const currentPoint = keywordAP
          ? manipulationCustomer.find(({ id }) => id == keywordAP)
              .customer_members
          : manipulationCustomer;

        setMarkers(currentPoint);

        populateSummary(currentPoint);
      })
      .catch((err) => console.error(err));
  };

  const getCustomerMember = async () => {
    await axios
      .get(`${SERVER_URL}/api/customer-member`)
      .then((response) => {
        const manipulationCustomerMember = response.data.map((ap) => ({
          ...ap,
          color:
            ap.rate >= 90
              ? 'text-green-500'
              : ap.rate < 70
              ? 'text-red-500'
              : 'text-orange-500',
          marker:
            ap.rate >= 90
              ? 'location-on-green'
              : ap.rate < 70
              ? 'location-on-red'
              : 'location-on-orange',
        }));

        setCustomerMembers(manipulationCustomerMember);
      })
      .catch((err) => console.error(err));
  };

  useEffect(() => {
    getCustomer();
    getCustomerMember();
  }, []);

  const tableColumns = [
    {
      title: 'Ranking',
      align: 'center',
      render: (column, row, index) => index + 1,
    },
    {
      title: 'Nama PPL',
      render: (colum, row) => row.name,
    },
    {
      title: 'Rata - Rata Rating Kepuasan',
      render: (colum, row) => `${row.rate} %`,
    },
    {
      title: 'Periode',
      render: (colum, row) => row.periode,
    },
    {
      title: 'Unit yang di Pegang',
      render: (colum, row) => row.parent.name,
    },
    {
      title: 'Rata - Rata IP',
      render: (colum, row) => row.avg_ip,
    },
    {
      title: 'Rata - Rata FCR',
      render: (colum, row) => row.avg_fcr,
    },
    {
      title: 'Populasi ekor yang dipegang',
      align: 'right',
      render: (colum, row) => formatNumber(row.qty),
    },
    {
      title: 'Rata - Rata Keuntungan Per Ekor',
      align: 'right',
      render: (colum, row) => formatNumber(row.avg_profit),
    },
  ];

  return (
    <div>
      {/* MAPS */}
      <MapContainer
        center={[-7.351802538696337, 108.22038827244134]}
        zoom={7.3}
        zoomControl={false}
        className="w-full h-[77vh] z-10 border-2 border-white shadow-xl"
      >
        <TileLayer
          url="https://tile.openstreetmap.org/{z}/{x}/{y}.png"
          attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        />

        {markers.map((marker) => (
          <Marker
            children={<div className="z-10">CHild</div>}
            eventHandlers={{
              dblclick: () => {
                if (!marker.parent) {
                  setKeywordAP(marker.id);
                  params.set('ap', marker.id);
                  window.history.replaceState(
                    {},
                    null,
                    `?${params.toString()}`
                  );

                  const clickedCustomer = customerMembers.filter(
                    ({ customer_id }) => customer_id == marker.id
                  );
                  setMarkers(clickedCustomer);
                  populateSummary(clickedCustomer);
                }
              },
            }}
            position={[marker.lat, marker.long]}
            icon={
              new Icon({
                iconUrl: `/icon/${marker.marker}.png`,
                iconSize: [38, 38],
              })
            }
          >
            <Popup closeButton={false}>
              <span className={`font-semibold ${marker.color}`}>
                <Circle fontSize="small" className="mr-2" />
                {marker.name}
              </span>
            </Popup>
          </Marker>
        ))}
      </MapContainer>

      {/* SUMMARY CARD */}
      <div className="w-[17%] text-center z-10 absolute top-28 left-5 flex flex-col gap-y-3">
        {summary.map((each) => (
          <div className="bg-white border border-slate-200 rounded-xl">
            <div className="my-2 text-xs font-medium text-slate-600">
              {each.title}
            </div>
            <Divider />
            <div className={`my-3 text-xl font-semibold ${each.color}`}>
              {each.content}
            </div>
          </div>
        ))}
      </div>

      {/* NAVIGATION */}
      <div className="flex justify-around w-full bg-white border-2 border-teal-600 text-teal-600 text-sm font-semibold rounded-s-full rounded-e-full overflow-hidden my-5">
        <button
          className={`w-full py-2 ${!keywordAP && 'bg-teal-600 text-white'}`}
          onClick={() => {
            setKeywordAP('');
            params.set('ap', '');
            window.history.replaceState({}, null, `?${params.toString()}`);

            setMarkers(customers);
            populateSummary(customers);
          }}
        >
          Semua AP
        </button>
        {customers.map((customer) => (
          <button
            className={`w-full py-2 ${
              keywordAP == customer.id && 'bg-teal-600 text-white'
            }`}
            onClick={() => {
              // UNTUK RESPONSIF
              setKeywordAP(customer.id);

              // UNTUK MEMPERTAHANKAN NILAI URL KETIKA DI REFRESH
              params.set('ap', customer.id);
              window.history.replaceState({}, null, `?${params.toString()}`);

              const clickedCustomer = customerMembers.filter(
                ({ customer_id }) => customer_id == customer.id
              );
              setMarkers(clickedCustomer);
              populateSummary(clickedCustomer);
            }}
          >
            {customer.name}
          </button>
        ))}
      </div>

      {/* TABLE */}
      <Table
        columns={tableColumns}
        bordered
        title={() => (
          <>
            <div className="text-xl font-medium">
              {keywordAP
                ? `Tabel Data ${
                    customers.find(({ id }) => id == keywordAP)?.name
                  }`
                : 'Tabel Data Semua Perusahaan'}
            </div>
            <Divider />
            <div className="bg-teal-600 bg-opacity-10 text-blue-700 text-xs p-2 w-fit rounded-md">
              Total data :{' '}
              {customerMembers.filter(
                ({ customer_id }) => customer_id == params.get('ap')
              ).length || customerMembers.length}
            </div>
          </>
        )}
        dataSource={
          keywordAP
            ? customerMembers.filter(
                ({ customer_id }) => customer_id == keywordAP
              )
            : customerMembers
        }
      />
    </div>
  );
}
