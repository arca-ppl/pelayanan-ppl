import React from 'react';
import { Navigate, Route, Routes } from 'react-router-dom';

import routes from './../routes';

export default function Content() {
  return (
    <Routes>
      {routes.map((route, index) => (
        <Route
          key={index}
          path={route.path}
          name={route.name}
          element={<route.element />}
        />
      ))}
      <Route path="*" element={<Navigate to="/ppl" replace />} />
    </Routes>
  );
}
