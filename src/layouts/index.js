import React from 'react';
import Content from './content';
import moment from 'moment';

export default function Index() {
  return (
    <div className="w-full h-screen bg-slate-100">
      <div className="w-[90%] mx-auto relative">
        <div className="text-2xl font-medium">Pelayanan PPL</div>
        <div className="bg-white text-center shadow">
          Tanggal Review {moment().format('DD-MM-YYYY')}
        </div>
        <div className="text-xs ">* Click pada Semua AP untuk nama AP</div>
        <div className="text-xs ">
          * Double click pada Semua AP untuk ke titik detail
        </div>
        <Content />
      </div>
    </div>
  );
}
